//
//  STTask+Mapping.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTask+Mapping.h"

@implementation STTask (Mapping)

- (void)mapWithSyncResponseOperation:(STSyncResponseOperation*)operation dateFormatter:(ISO8601DateFormatter*)dateFormatter
{
	[super mapWithSyncResponseOperation:operation dateFormatter:dateFormatter];
	self.name = operation.data[@"task"];
	self.dueAt = [dateFormatter dateFromString:operation.data[@"due"]];
	self.completed = operation.data[@"completed"];
}

- (NSDictionary*)dictionaryWithDateFormatter:(ISO8601DateFormatter*)dateFormatter
{
//	NSMutableDictionary* dictionary = [[super dictionaryWithDateFormatter:dateFormatter] mutableCopy];
//	[dictionary addEntriesFromDictionary:@{
//		@"task":self.name,
//		@"due":[dateFormatter stringFromDate:self.dueAt],
//		@"completed":self.completed,
//		@"sentreminder":@(NO), //Temp
//		@"assigneeId":@(1), //Temp
//	}];
//	return [dictionary copy];
	
	return @{
		   @"task":self.name,
		   @"due":[dateFormatter stringFromDate:self.dueAt],
		   @"completed":self.completed,
		   @"sentReminder":@(NO), //Temp
		   @"assigneeId":@(1), //Temp
	};
}

@end
