//
//  STSyncRequestOperation.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ISO8601DateFormatter.h"
#import <BloodMagic/Lazy.h>
#import "STSyncService.h"
#import "STBaseEntity.h"

@interface STSyncRequestOperation : NSObject <BMLazy>

@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;
@property (strong, nonatomic) Class type;
@property (strong, nonatomic) NSNumber* serverId;
@property (copy, nonatomic) NSString* guid;
@property (strong, nonatomic) NSDate* createdAt;
@property (strong, nonatomic) NSDate* updatedAt;
@property (assign, nonatomic) STOperationType operation;
@property (strong, nonatomic) STBaseEntity* object;

+ (instancetype)operationForObject:(NSManagedObject*)object operation:(STOperationType)operation;
- (instancetype)initWithObject:(NSManagedObject*)object operation:(STOperationType)operation;
- (void)mapWithObject:(NSManagedObject*)object operation:(STOperationType)operation;
- (NSDictionary*)dictionary;

/*
{
	"op":"create",
	"created":"2014-08-21T16:06:45Z",
	"guid":"6AA5E540-033A-4C0D-B2D8-8CF70A153680",
	"type":"task",
	"data":
	{
		"due":"2014-08-22T16:06:45Z",
		"task":"Chips",
		"completed":0
	}
}
*/

@end
