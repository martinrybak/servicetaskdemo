//
//  STInitializer.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/11/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STInitializer.h"
#import "MRHTTPClient.h"
#import "STHTTPClient.h"
#import "MockSTHTTPClient.h"
#import "GSKeychain.h"
#import "ISO8601DateFormatter.h"
#import "STSyncService.h"
#import "STAuthService.h"
#import "STTaskService.h"
#import "BMInitializer+Helpers.h"

#if DEBUG
NSString* const baseURLString = @"https://local.servicetask.com:6789";
//NSString* const syncBaseURLString = @"http://localhost:9001";
NSString* const syncBaseURLString = @"http://10.3.0.99:9001";
#endif

@implementation STInitializer

/**
 *  Bootstraps instances for dependency injection
 */
+ (void)initialize
{
	//GSKeychain
	[BMInitializer initializeInstanceForClass:[GSKeychain class] block:^id{
		return [GSKeychain systemKeychain];
	}];
	
	//ISO8601DateFormatter
	[BMInitializer initializeInstanceForClass:[ISO8601DateFormatter class] block:^id{
		static ISO8601DateFormatter* dateFormatter = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			dateFormatter = [[ISO8601DateFormatter alloc] init];
			dateFormatter.includeTime = YES;
		});
		return dateFormatter;
	}];
	
	//MRCoreDataStore
	[BMInitializer initializeInstanceForClass:[MRCoreDataStore class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[MRCoreDataStore alloc] init];
		});
		return singleInstance;
	}];
		
	//STAuthService
	[BMInitializer initializeInstanceForClass:[STAuthService class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[STAuthService alloc] init];
		});
		return singleInstance;
	}];
	
	//STTaskService
	[BMInitializer initializeInstanceForClass:[STTaskService class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[STTaskService alloc] init];
		});
		return singleInstance;
	}];
	
	//STSyncService
	[BMInitializer initializeInstanceForClass:[STSyncService class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[STSyncService alloc] init];
		});
		return singleInstance;
	}];
	
	//MRHTTPClient
	[BMInitializer initializeInstanceForClass:[MRHTTPClient class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[MRHTTPClient alloc] init];
		});
		return singleInstance;
	}];
	
	//STHTTPClient for STSyncService
	[BMInitializer initializeInstanceForClass:[STHTTPClient class] container:[STSyncService class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[STHTTPClient alloc] initWithBaseURL:syncBaseURLString];
		});
		return singleInstance;
	}];
	
	//STHTTPClient
	[BMInitializer initializeInstanceForClass:[STHTTPClient class] block:^id{
		static id singleInstance = nil;
		static dispatch_once_t once;
		dispatch_once(&once, ^{
			singleInstance = [[STHTTPClient alloc] initWithBaseURL:baseURLString];
		});
		return singleInstance;
	}];
}

@end
