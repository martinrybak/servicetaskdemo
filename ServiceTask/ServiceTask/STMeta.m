//
//  STMeta.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STMeta.h"


@implementation STMeta

@dynamic lastSyncedAt;
@dynamic tokenExpiresAt;
@dynamic tokenIssuedAt;

@end
