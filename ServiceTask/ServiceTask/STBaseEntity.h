//
//  STBaseEntity.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/28/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "ISO8601DateFormatter.h"

@class STSyncResponseOperation;

typedef NS_ENUM(NSUInteger, STOperationType) {
	STOperationTypeNone = 0,
    STOperationTypeCreate = 1,
    STOperationTypeUpdate = 2,
    STOperationTypeDelete = 3
};

@interface STBaseEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * serverId;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSNumber * operation;

- (void)mapWithSyncResponseOperation:(STSyncResponseOperation*)operation dateFormatter:(ISO8601DateFormatter*)dateFormatter;
- (NSDictionary*)dictionaryWithDateFormatter:(ISO8601DateFormatter*)dateFormatter;
- (void)clearOperation;
- (void)create;
- (void)update;
- (void)delete;
- (BOOL)shouldHardDelete;

@end
