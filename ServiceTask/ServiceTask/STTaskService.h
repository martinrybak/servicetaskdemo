//
//  STTaskService.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/26/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRCoreDataStore.h"
#import "STHTTPClient.h"
#import "ISO8601DateFormatter.h"
#import <BloodMagic/Lazy.h>
#import "STTask.h"
#import "STSyncService.h"

@interface STTaskService : NSObject <BMLazy>

@property (weak, nonatomic) MRCoreDataStore* store;
@property (weak, nonatomic) STHTTPClient* client;
@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;
@property (weak, nonatomic) STSyncService* syncService;

- (NSFetchedResultsController*)tasksFetchedResultsController;
- (void)createTaskWithName:(NSString*)name;
- (void)updateTask:(STTask*)task withName:(NSString*)name;
- (void)deleteTask:(STTask*)task;

@end
