//
//  NSManagedObjectContext+Async.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Async)

- (void)executeFetchRequest:(NSFetchRequest*)request success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure;

@end
