//
//  MRHTTPClient.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/19/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRHTTPClient : NSObject

- (void)send:(NSURLRequest*)request success:(void(^)(NSHTTPURLResponse* response, NSData* data))success failure:(void(^)(NSError* error))failure;

@end
