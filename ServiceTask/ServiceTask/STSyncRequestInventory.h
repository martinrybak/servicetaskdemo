//
//  STSyncRequestInventory.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ISO8601DateFormatter.h"

@interface STSyncRequestInventory : NSObject

@property (strong, nonatomic) NSNumber* serverId;
@property (copy, nonatomic) NSString* guid;
@property (strong, nonatomic) Class type;

+ (instancetype)inventoryForObject:(NSManagedObject*)object;
- (instancetype)initWithObject:(NSManagedObject*)object;
- (void)mapWithObject:(NSManagedObject*)object;
- (NSDictionary*)dictionary;

@end
