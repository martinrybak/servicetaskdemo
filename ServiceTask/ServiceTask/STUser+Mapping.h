//
//  STUser+Mapping.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/8/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STUser.h"
#import "ISO8601DateFormatter.h"

@interface STUser (Mapping)

- (void)mapWithDictionary:(NSDictionary*)dictionary dateFormatter:(ISO8601DateFormatter*)dateFormatter;

@end
