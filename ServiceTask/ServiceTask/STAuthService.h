//
//  STAuthService.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/26/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRCoreDataStore.h"
#import "STHTTPClient.h"
#import "ISO8601DateFormatter.h"
#import "GSKeychain.h"
#import <BloodMagic/Lazy.h>
#import "STUser.h"

@interface STAuthService : NSObject <BMLazy>

@property (weak, nonatomic) MRCoreDataStore* store;
@property (weak, nonatomic) STHTTPClient* client;
@property (weak, nonatomic) GSKeychain* keychain;
@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;

- (void)loginWithEmail:(NSString*)email password:(NSString*)password success:(void(^)(void))success failure:(void(^)(NSError* error))failure;
- (NSString*)token;

@end
