//
//  STHTTPClient.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STHTTPClient.h"
#import "NSURLRequest+SSL.h"
#import "CMDQueryStringSerialization.h"
#import "NSDictionary+Helpers.h"
#import "NSObject+JSON.h"
#import "NSData+CSRF.h"

@implementation STHTTPClient

@dynamic client;

#pragma mark - NSObject

- (id)initWithBaseURL:(NSString*)baseURLString
{
	if (self = [super init]) {
		_baseURL = [NSURL URLWithString:baseURLString];
		#if DEBUG
		[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:_baseURL.host];
		#endif
	}
	return self;
}

#pragma mark - Public

- (void)get:(NSString*)endpoint query:(NSDictionary*)query cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure
{
	NSMutableURLRequest* request = [self requestForEndpoint:endpoint query:query cookie:cookie];
	[self sendRequest:request success:success failure:failure];
}

- (void)post:(NSString*)endpoint query:(NSDictionary*)query body:(NSDictionary*)body cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure
{
	NSMutableURLRequest* request = [self requestForEndpoint:endpoint query:query cookie:cookie];
	request.HTTPMethod = @"POST";
	request.HTTPBody = [body JSON];
	NSString* print = [[NSString alloc] initWithData:[body JSON] encoding:NSUTF8StringEncoding];
	[self sendRequest:request success:success failure:failure];
}

#pragma mark - Private

- (NSMutableURLRequest*)requestForEndpoint:(NSString*)endpoint query:(NSDictionary*)query cookie:(NSString*)cookie
{
	//Construct the URL + querystring (if provided)
	NSString* path;
	if (query) {
		NSString* queryString = [CMDQueryStringSerialization queryStringWithDictionary:query];
		path = [NSString stringWithFormat:@"%@?%@", endpoint, queryString];
	} else {
		path = endpoint;
	}
	
	NSURL* url = [NSURL URLWithString:path relativeToURL:self.baseURL];
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
	
	//Set no cache policy
	request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
	
	//Set content type
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	
	//Set cookie, if exists
	if (cookie) {
		[request setValue:cookie forHTTPHeaderField:@"Cookie"];
	}
	
	return request;
}

- (void)sendRequest:(NSURLRequest*)request success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure
{
	[self.client send:request success:^(NSHTTPURLResponse* response, NSData* data) {
		
		#if DEBUG
		NSString* json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		NSLog(@"%@", json);
		#endif
		
		//Remove the CSRF prefix
		NSData* cleanedData = [data removeCSRFPrefix];
		
		//Parse JSON response
		NSError* error;
		id object = [NSJSONSerialization JSONObjectWithData:cleanedData options:0 error:&error];
		if (error) {
			if (failure) {
				failure(error);
			}
			return;
		}
		
		if (success) {
			success(response.allHeaderFields, object);
		}
	} failure:failure];
}

@end
