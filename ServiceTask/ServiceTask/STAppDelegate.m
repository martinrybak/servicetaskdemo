//
//  STAppDelegate.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STAppDelegate.h"
#import "STLoginViewController.h"
#import "STTasksViewController.h"
#import "MRNavigationController.h"
#import "Reachability.h"
#import "JDStatusBarNotification.h"
#import "MBProgressHUD.h"

@interface STAppDelegate ()

@property (strong, nonatomic) MRNavigationController* navigationController;
@property (strong, nonatomic) Reachability* reachability;

@end

@implementation STAppDelegate

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	//Set up main window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
	
	//Set up tasks view controller
	STTasksViewController* tasksViewController = [self loadViewController:[STTasksViewController class]];
	tasksViewController.title = @"My Tasks";
	UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:tasksViewController action:@selector(addTask)];
	tasksViewController.navigationItem.rightBarButtonItem = addButton;
	
	STLoginViewController* loginViewController = [self loadViewController:[STLoginViewController class]];
	loginViewController.title = @"Login";
	
	//Embed in navigation controller and make it the window root view controller
	self.navigationController = [self embedViewControllerInNavigationController:tasksViewController navigationBarHidden:NO];
    self.window.rootViewController = self.navigationController;
	
	// Set up reachability
	#if !TARGET_IPHONE_SIMULATOR
	self.reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
	self.reachability.reachableBlock = ^(Reachability* reachability)
	{
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			[JDStatusBarNotification dismiss];
		}];
	};
	self.reachability.unreachableBlock = ^(Reachability* reachability)
	{
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			[JDStatusBarNotification showWithStatus:@"Offline mode" styleName:JDStatusBarStyleError];
		}];
	};
	[self.reachability startNotifier];
	#endif
	
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - STBaseViewControllerDelegate

- (BOOL)online
{
	#if TARGET_IPHONE_SIMULATOR
	return YES;
	#endif
	
	return self.reachability.isReachable;
}

- (void)busy:(BOOL)busy
{
	if (busy) {
        MBProgressHUD* hud = [[MBProgressHUD alloc] initWithWindow:self.window];
		hud.color = [UIColor grayColor];
		hud.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
        hud.minShowTime = 1.0;
        [self.window addSubview:hud];
        [hud show:YES];
    }
	else {
        [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
	}
}

- (void)failure:(NSError*)error
{
	[[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark - Navigation

- (id)loadViewController:(Class)class
{
	id viewController = [[class alloc] init];
	[viewController setValue:self forKey:@"delegate"];
	return viewController;
}

- (id)embedViewControllerInNavigationController:(UIViewController*)viewController navigationBarHidden:(BOOL)navigationBarHidden
{
	return [self embedViewControllerInNavigationController:viewController navigationBarHidden:navigationBarHidden barTintColor:[UIColor blueColor]];
}

- (id)embedViewControllerInNavigationController:(UIViewController*)viewController navigationBarHidden:(BOOL)navigationBarHidden barTintColor:(UIColor*)barTintColor
{
	MRNavigationController* navigationController = [[MRNavigationController alloc] initWithRootViewController:viewController navigationBarHidden:navigationBarHidden toolbarHidden:YES];
	return navigationController;
}

- (void)pushViewController:(UIViewController*)viewController ontoNavigationController:(MRNavigationController*)navigationController
{
	[self pushViewController:viewController ontoNavigationController:navigationController animated:YES];
}

- (void)pushViewController:(UIViewController*)viewController ontoNavigationController:(MRNavigationController*)navigationController animated:(BOOL)animated
{
	[self pushViewController:viewController ontoNavigationController:navigationController animated:animated navigationBarHidden:NO];
}

- (void)pushViewController:(UIViewController*)viewController ontoNavigationController:(MRNavigationController*)navigationController animated:(BOOL)animated navigationBarHidden:(BOOL)navigationBarHidden
{
	[navigationController pushViewController:viewController animated:animated navigationBarHidden:navigationBarHidden toolbarHidden:YES push:nil pop:nil];
}

- (void)presentModalViewController:(UIViewController*)viewController fromViewController:(UIViewController*)fromViewController
{
	[self presentModalViewController:viewController fromViewController:fromViewController animated:YES completion:nil];
}

- (void)presentModalViewController:(UIViewController*)viewController fromViewController:(UIViewController*)fromViewController animated:(BOOL)animated completion:(void (^)(void))completion
{
	//Load and configure the container navigation controller
	MRNavigationController* navigationController = [self embedViewControllerInNavigationController:viewController navigationBarHidden:NO];
	
	//Configure and present the view controller
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(dismissViewController)];
	viewController.navigationItem.leftBarButtonItem = cancelButton;
	viewController.navigationItem.titleView = nil;
	
	//Present the view controller
	[fromViewController presentViewController:navigationController animated:animated completion:completion];
}

- (void)presentViewController:(UIViewController*)viewController fromViewController:(UIViewController*)fromViewController
{
	[fromViewController presentViewController:viewController animated:YES completion:nil];
}

- (void)dismissViewController
{
	[self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
