//
//  STSyncRequest.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISO8601DateFormatter.h"
#import <BloodMagic/Lazy.h>

@interface STSyncRequest : NSObject <BMLazy>

@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;
@property (strong, nonatomic) NSDate* lastSyncDate;
@property (strong, nonatomic) NSDate* from;
@property (strong, nonatomic) NSDate* to;
@property (strong, nonatomic) NSArray* inventory;
@property (strong, nonatomic) NSArray* creates;
@property (strong, nonatomic) NSArray* updates;
@property (strong, nonatomic) NSArray* deletes;

+ (instancetype)requestWithInventory:(NSArray*)inventory creates:(NSArray*)creates updates:(NSArray*)updates deletes:(NSArray*)deletes from:(NSDate*)from to:(NSDate*)to lastSyncDate:(NSDate*)lastSyncDate;
- (instancetype)initWithInventory:(NSArray*)inventory creates:(NSArray*)creates updates:(NSArray*)updates deletes:(NSArray*)deletes from:(NSDate*)from to:(NSDate*)to lastSyncDate:(NSDate*)lastSyncDate;
- (NSDictionary*)dictionary;

@end
