//
//  STTask.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTask.h"

@implementation STTask

@dynamic completed;
@dynamic dueAt;
@dynamic name;

@end
