//
//  STBaseViewController.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STBaseViewController.h"

@implementation STBaseViewController

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder*)aDecoder
{
	if (self = [super initWithCoder:aDecoder]) {
		[self setup];
	}
	return self;
}

#pragma mark - UIViewController

- (instancetype)initWithNibName:(NSString*)nibNameOrNil bundle:(NSBundle*)nibBundleOrNil
{
	if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		[self setup];
	}
	return self;
}

#pragma mark - Public

- (void)setup
{
}

@end
