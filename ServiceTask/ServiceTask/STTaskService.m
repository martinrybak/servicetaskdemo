//
//  STTaskService.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/26/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTaskService.h"
#import "NSArray+LinqExtensions.h"
#import "NSDate+Add.h"
#import "STSyncService.h" //Should we move STSyncOperationType somewhere else?

@implementation STTaskService

@dynamic store;
@dynamic client;
@dynamic dateFormatter;
@dynamic syncService;

- (NSFetchedResultsController*)tasksFetchedResultsController
{
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"operation != %@", @(STOperationTypeDelete)];
    NSSortDescriptor* sort = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
	return [self.store fetchedResultsControllerForType:[STTask class] predicate:predicate sortDescriptors:@[ sort ]];
}

- (void)createTaskWithName:(NSString*)name
{
	[self.store updateAndCommitWithBlock:^{
		STTask* task = [self.store createObjectOfType:[STTask class]];
		task.name = name;
		task.dueAt = [[NSDate date] addDays:7];
		task.completed = @(NO);
		[task create];
	} success:^{
		[self.syncService sync:nil failure:nil];
	} failure:nil];
}

- (void)updateTask:(STTask*)task withName:(NSString*)name
{
	[self.store updateAndCommitWithBlock:^{
		STTask* localTask = [self.store findObjectById:task.objectID];
		localTask.name = name;
		[localTask update];
	} success:^{
		[self.syncService sync:nil failure:nil];
	} failure:nil];
}

- (void)deleteTask:(STTask*)task
{	
	[self.store updateAndCommitWithBlock:^{
		STTask* localTask = [self.store findObjectById:task.objectID];
		[localTask delete];
		if ([localTask shouldHardDelete]) {
			[self.store deleteObject:localTask];
		}
	} success:^{
		if (![task shouldHardDelete]) {
			[self.syncService sync:nil failure:nil];
		}
	} failure:nil];
}

@end
