//
//  NSDictionary+Helpers.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/8/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "NSDictionary+Helpers.h"

@implementation NSDictionary (Helpers)

- (BOOL)isEmpty
{
	return [self allKeys].count == 0;
}

@end
