//
//  STUser+Helpers.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STUser.h"

@interface STUser (Helpers)

- (NSString*)fullName;

@end
