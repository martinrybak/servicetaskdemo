//
//  NSArray+Concatenate.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Concatenate)

+ (NSArray*)concatenate:(NSArray*)arrays;
- (NSArray*)concatenate:(NSArray*)array;

@end
