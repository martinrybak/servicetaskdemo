//
//  STHTTPClient.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRHTTPClient.h"
#import <BloodMagic/Lazy.h>

@interface STHTTPClient : NSObject <BMLazy>

@property (copy, nonatomic) NSURL* baseURL;
@property (weak, nonatomic) MRHTTPClient* client;

- (id)initWithBaseURL:(NSString*)baseURLString;
- (void)get:(NSString*)endpoint query:(NSDictionary*)query cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure;
- (void)post:(NSString*)endpoint query:(NSDictionary*)query body:(NSDictionary*)body cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure;

@end
