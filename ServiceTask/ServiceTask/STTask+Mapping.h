//
//  STTask+Mapping.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTask.h"
#import "ISO8601DateFormatter.h"
#import "STSyncResponseOperation.h"

@interface STTask (Mapping)

- (void)mapWithSyncResponseOperation:(STSyncResponseOperation*)operation dateFormatter:(ISO8601DateFormatter*)dateFormatter;
- (NSDictionary*)dictionaryWithDateFormatter:(ISO8601DateFormatter*)dateFormatter;

@end
