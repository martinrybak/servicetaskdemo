//
//  STUser.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface STUser : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;

@end
