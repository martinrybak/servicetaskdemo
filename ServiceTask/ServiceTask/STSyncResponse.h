//
//  STSyncResponse.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BloodMagic/Lazy.h>
#import "ISO8601DateFormatter.h"

@interface STSyncResponse : NSObject <BMLazy>

@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;
@property (strong, nonatomic) NSDate* timestamp;
@property (strong, nonatomic) NSDate* from;
@property (strong, nonatomic) NSDate* to;
@property (strong, nonatomic) NSArray* operations;
@property (strong, nonatomic) NSArray* errors;

+ (instancetype)responseWithDictionary:(NSDictionary*)dictionary;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
- (void)mapWithDictionary:(NSDictionary*)dictionary;

@end
