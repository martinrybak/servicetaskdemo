//
//  STSyncService.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRCoreDataStore.h"
#import "STHTTPClient.h"
#import "ISO8601DateFormatter.h"
#import <BloodMagic/Lazy.h>
#import "STBaseEntity.h"

@interface STSyncService : NSObject <BMLazy>

@property (weak, nonatomic) MRCoreDataStore* store;
@property (weak, nonatomic) STHTTPClient* client;
@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;

+ (Class)classForString:(NSString*)string;
+ (NSString*)stringForClass:(Class)class;
+ (STOperationType)operationForString:(NSString*)string;
+ (NSString*)stringForOperation:(STOperationType)operation;
- (void)sync:(void(^)(void))success failure:(void(^)(NSError* error))failure;
- (NSDate*)lastSyncDate;

@end
