//
//  STSyncRequestInventory.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STSyncRequestInventory.h"
#import "STSyncService.h"

@implementation STSyncRequestInventory

+ (instancetype)inventoryForObject:(NSManagedObject*)object
{
	return [[self alloc] initWithObject:object];
}

- (instancetype)initWithObject:(NSManagedObject*)object
{
	if (self = [super init]) {
		[self mapWithObject:object];
	}
	return self;
}

- (void)mapWithObject:(NSManagedObject*)object
{
	self.serverId = [object valueForKey:@"serverId"];
	self.guid = [object valueForKey:@"guid"];
	self.type = [object class];
}

- (NSDictionary*)dictionary
{
	NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
	dictionary[@"type"] = [STSyncService stringForClass:self.type];
	
	if (self.serverId.integerValue) {
		dictionary[@"id"] = self.serverId;
	} else {
		dictionary[@"guid"] = self.guid;
	}
	return [dictionary copy];
}

@end
