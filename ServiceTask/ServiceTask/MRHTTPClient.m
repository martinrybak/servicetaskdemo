//
//  MRHTTPClient.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/19/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "MRHTTPClient.h"

@implementation MRHTTPClient

- (void)send:(NSURLRequest*)request success:(void(^)(NSHTTPURLResponse* response, NSData* data))success failure:(void(^)(NSError* error))failure
{
	//Show iOS activity indicator
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	}];
	
	//Process response on a new queue, calls back on callback queue
	[NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError) {
		
		//Hide iOS activity indicator
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		}];
		
		//Failure
		if (connectionError) {
			if (failure) {
				failure(connectionError);
			}
			return;
		}
		
		//Success
		if (success) {
			success((NSHTTPURLResponse*)response, data);
		}
	}];
}

@end
