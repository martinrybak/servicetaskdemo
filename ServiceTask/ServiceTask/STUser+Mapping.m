//
//  STUser+Mapping.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/8/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STUser+Mapping.h"

@implementation STUser (Mapping)

- (void)mapWithDictionary:(NSDictionary*)dictionary dateFormatter:(ISO8601DateFormatter*)dateFormatter
{
	self.email = dictionary[@"email"];
	self.firstName = dictionary[@"firstname"];
	self.lastName = dictionary[@"lastname"];
}

@end
