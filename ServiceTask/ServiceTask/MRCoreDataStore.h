//
//  MRCoreDataStore.h
//
//  Created by Martin Rybak on 6/30/14.
//  Copyright (c) 2014 Martin Rybak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

/**
 *  This Core Data stack contains two internally managed object contexts:
 *  -Foreground: Read-only. For UI. Access from main thread only.
 *  -Background: Read-and-Write. Access from the background thread's private queue only.
 *
 *  Generally, the consumer does not need to be aware that these dual contexts exist.
 *  The consumer simply needs to
 */
@interface MRCoreDataStore : NSObject

@property (strong, nonatomic) NSManagedObjectModel* managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator* persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectContext* foregroundContext;
@property (strong, nonatomic) NSManagedObjectContext* backgroundContext;

#pragma mark - Override hooks for subclasses

/**
 *  Returns the directory where the <kCFBundleIdentifierKey>.sqlite file will be stored. Subclass to override.
 *
 *  @return NSURL
 */
- (NSURL*)databaseDirectory;

/**
 *  Returns the options for the SQLite peristent store. Subclass to override. Default is:
 *	-Automatically attempt to migrate versioned stores
 *  -Automatically attempt to infer a mapping model if none can be found
 *  -Enable write-ahead logging
 *
 *  @return NSDictionary
 */
- (NSDictionary*)storeOptions;

#pragma mark - Main or background queue usage only

- (NSFetchRequest*)fetchRequestForType:(Class)class;
- (NSFetchRequest*)fetchRequestForType:(Class)class predicate:(NSPredicate*)predicate;
- (NSFetchRequest*)fetchRequestForType:(Class)class predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors;
- (NSFetchedResultsController*)fetchedResultsControllerForType:(Class)class;
- (NSFetchedResultsController*)fetchedResultsControllerForType:(Class)class predicate:(NSPredicate*)predicate;
- (NSFetchedResultsController*)fetchedResultsControllerForType:(Class)class predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors;
- (NSFetchedResultsController*)fetchedResultsControllerWithFetchRequest:(NSFetchRequest*)fetchRequest;

- (id)findObjectById:(NSManagedObjectID*)objectId;
- (id)findObjectOfType:(Class)class byAttribute:(NSString*)attribute withValue:(id)value;
- (id)findObjectOfType:(Class)class byPredicate:(NSPredicate*)predicate;
- (NSArray*)findObjectsOfType:(Class)class;
- (NSArray*)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate;
- (NSArray*)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors;

#pragma mark - Main thread usage only

- (void)findObjectsOfType:(Class)class success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure;
- (void)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure;
- (void)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure;
- (void)updateAndCommitWithBlock:(void(^)(void))block success:(void(^)(void))success failure:(void(^)(NSError* error))failure;
- (void)updateWithBlock:(void(^)(void))block completion:(void(^)(void))completion;
- (void)commitWithSuccess:(void(^)(void))success failure:(void(^)(NSError* error))failure;
- (void)rollbackWithCompletion:(void(^)(void))completion;

#pragma mark - Background thread usage only

- (id)findOrCreateObjectOfType:(Class)class byAttribute:(NSString*)attribute withValue:(id)value;
- (id)createObjectOfType:(Class)class;
- (void)deleteObject:(NSManagedObject*)object;
- (void)deleteObjectOfType:(Class)class byAttribute:(NSString*)attribute withValue:(id)value;
- (void)deleteObjectsOfType:(Class)class;
- (void)deleteObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate;

@end