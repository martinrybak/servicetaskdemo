//
//  STAppDelegate.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STBaseViewController.h"

@interface STAppDelegate : UIResponder <UIApplicationDelegate, STBaseViewControllerDelegate>

@property (strong, nonatomic) UIWindow* window;

@end
