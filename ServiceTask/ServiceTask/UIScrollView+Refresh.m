//
//  UIScrollView+Refresh.m
//  Updated
//
//  Created by Martin Rybak on 3/15/14.
//  Copyright (c) 2014 Updated. All rights reserved.
//

#import "UIScrollView+Refresh.h"
#import <objc/runtime.h>

void* kRefreshControlKey;
void* kRefreshControlBackgroundKey;

@implementation UIScrollView (Refresh)

@dynamic refreshControlBackground;
@dynamic refreshControl;

- (void)addRefreshControlWithTarget:(id)target action:(SEL)action
{
	[self addRefreshControlWithTarget:target action:action backgroundColor:[UIColor clearColor]];
}

- (void)addRefreshControlWithTarget:(id)target action:(SEL)action backgroundColor:(UIColor*)backgroundColor
{
    //Create a pull-to-refresh spinner and adjust scrollView contentSize to contain it
	self.refreshControl = [[UIRefreshControl alloc] init];
    [self addSubview:self.refreshControl];
	[self.refreshControl addTarget:target action:action forControlEvents:UIControlEventValueChanged];
	
	// Creating view for extending background color
    CGRect frame = self.bounds;
    frame.origin.y = -frame.size.height;
    self.refreshControlBackground = [[UIView alloc] initWithFrame:frame];
    self.refreshControlBackground.backgroundColor = backgroundColor;
	
    // Adding the view below the refresh control
    [self insertSubview:self.refreshControlBackground atIndex:0];
    
    if ([[self class] isSubclassOfClass:[UITableView class]])
        return;
    
	self.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height + self.refreshControl.frame.size.height);
}

- (void)removeRefreshControl
{
	[self.refreshControl removeFromSuperview];
	[self.refreshControlBackground removeFromSuperview];
	self.refreshControl = nil;
	self.refreshControlBackground = nil;
}

- (void)endRefreshing
{
    [self.refreshControl endRefreshing];
}

- (void)setRefreshControl:(UIRefreshControl*)refreshControl
{
    objc_setAssociatedObject(self, &kRefreshControlKey, refreshControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIRefreshControl*)refreshControl
{
    return objc_getAssociatedObject(self, &kRefreshControlKey);
}

- (void)setRefreshControlBackground:(UIView*)refreshControlBackground
{
    objc_setAssociatedObject(self, &kRefreshControlBackgroundKey, refreshControlBackground, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView*)refreshControlBackground
{
    return objc_getAssociatedObject(self, &kRefreshControlBackgroundKey);
}

@end