//
//  STLoginViewController.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STBaseViewController.h"
#import <BloodMagic/Lazy.h>
#import "STAuthService.h"
#import "STSyncService.h"

@interface STLoginViewController : STBaseViewController <BMLazy>

@property (weak, nonatomic) STAuthService* authService;
@property (weak, nonatomic) STSyncService* syncService;

@end
