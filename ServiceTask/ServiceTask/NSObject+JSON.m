//
//  NSObject+JSON.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "NSObject+JSON.h"
#import "NSObject+MTJSONUtils.h"

@implementation NSObject (JSON)

- (NSData*)JSON
{
	return [NSJSONSerialization dataWithJSONObject:[self objectWithJSONSafeObjects] options:0 error:nil];
}

@end
