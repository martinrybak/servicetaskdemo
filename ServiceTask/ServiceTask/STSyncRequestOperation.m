//
//  STSyncRequestOperation.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STSyncRequestOperation.h"
#import "STSyncService.h"

@implementation STSyncRequestOperation

@dynamic dateFormatter;

+ (instancetype)operationForObject:(NSManagedObject*)object operation:(STOperationType)operation
{
	return [[self alloc] initWithObject:object operation:operation];
}

- (instancetype)initWithObject:(NSManagedObject*)object operation:(STOperationType)operation
{
	if (self = [super init]) {
		[self mapWithObject:object operation:operation];
	}
	return self;
}

- (void)mapWithObject:(STBaseEntity*)object operation:(STOperationType)operation
{
	self.serverId = [object valueForKey:@"serverId"];
	self.guid = [object valueForKey:@"guid"];
	self.type = [object class];
	self.operation = operation;
	self.object = object;
	self.createdAt = [object valueForKey:@"createdAt"];
	self.updatedAt = [object valueForKey:@"updatedAt"];
}

- (NSDictionary*)dictionary
{
	NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
	dictionary[@"type"] = [STSyncService stringForClass:self.type];
	dictionary[@"op"] = [STSyncService stringForOperation:self.operation];
	
	switch (self.operation)
	{
		case STOperationTypeCreate:
		{
			dictionary[@"guid"] = self.guid;
			dictionary[@"created"] = self.createdAt;
			dictionary[@"updated"] = self.updatedAt;
			dictionary[@"data"] = [self.object dictionaryWithDateFormatter:self.dateFormatter];
			break;
		}
		case STOperationTypeUpdate:
		{
			dictionary[@"id"] = self.serverId;
			dictionary[@"updated"] = self.updatedAt;
			dictionary[@"data"] = [self.object dictionaryWithDateFormatter:self.dateFormatter];
			break;
		}
		case STOperationTypeDelete:
		{
			dictionary[@"id"] = self.serverId;
			break;
		}
	}
	
	return [dictionary copy];
}

@end
