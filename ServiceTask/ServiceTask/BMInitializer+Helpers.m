//
//  BMInitializer+Helpers.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/26/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "BMInitializer+Helpers.h"

@implementation BMInitializer (Helpers)

#pragma mark - Public

+ (void)initializeInstanceForClass:(Class)propertyClass container:(Class)containerClass block:(id(^)(void))block
{
	BMInitializer* initializer = [BMInitializer lazyInitializer];
	initializer.propertyClass = propertyClass;
	if (containerClass) {
		initializer.containerClass = containerClass;
	}
	initializer.initializer = ^id (id sender){
		return block();
	};
	[initializer registerInitializer];
}

+ (void)initializeInstanceForClass:(Class)propertyClass block:(id(^)(void))block
{
	return [self initializeInstanceForClass:propertyClass container:nil block:block];
}

+ (void)initializeInstanceForProtocol:(Protocol*)protocol container:(Class)containerClass block:(id(^)(void))block
{
	BMInitializer* initializer = [BMInitializer lazyInitializer];
	initializer.protocols = @[ protocol ];
	if (containerClass) {
		initializer.containerClass = containerClass;
	}
	initializer.initializer = ^id (id sender){
		return block();
	};
	[initializer registerInitializer];
}

+ (void)initializeInstanceForProtocol:(Protocol*)protocol block:(id(^)(void))block
{
	return [self initializeInstanceForProtocol:protocol container:nil block:block];
}

@end
