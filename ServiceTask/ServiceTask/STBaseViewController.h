//
//  STBaseViewController.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BloodMagic/Lazy.h>

@protocol STBaseViewControllerDelegate <NSObject>

- (BOOL)online;
- (void)busy:(BOOL)busy;
- (void)failure:(NSError*)error;

@end

@interface STBaseViewController : UIViewController <BMLazy>

@property (weak, nonatomic) id <STBaseViewControllerDelegate> delegate;
- (void)setup;

@end
