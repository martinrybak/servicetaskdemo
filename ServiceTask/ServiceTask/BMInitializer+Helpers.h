//
//  BMInitializer+Helpers.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/26/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "BMInitializer.h"
#import <BloodMagic/Lazy.h>

@interface BMInitializer (Helpers)

+ (void)initializeInstanceForClass:(Class)propertyClass container:(Class)containerClass block:(id(^)(void))block;
+ (void)initializeInstanceForClass:(Class)propertyClass block:(id(^)(void))block;
+ (void)initializeInstanceForProtocol:(Protocol*)protocol container:(Class)containerClass block:(id(^)(void))block;
+ (void)initializeInstanceForProtocol:(Protocol*)protocol block:(id(^)(void))block;

@end
