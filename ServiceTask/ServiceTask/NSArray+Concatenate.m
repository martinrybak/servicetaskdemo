//
//  NSArray+Concatenate.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "NSArray+Concatenate.h"

@implementation NSArray (Concatenate)

+ (NSArray*)concatenate:(NSArray*)arrays
{
	NSArray* output = [[NSArray alloc] init];
	for (NSArray* array in arrays) {
		output = [output arrayByAddingObjectsFromArray:array];
	}
	return output;
}

- (NSArray*)concatenate:(NSArray*)array
{
	return [self arrayByAddingObjectsFromArray:array];
}

@end
