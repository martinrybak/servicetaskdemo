//
//  NSManagedObjectContext+Async.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "NSManagedObjectContext+Async.h"

@implementation NSManagedObjectContext (Async)

- (void)executeFetchRequest:(NSFetchRequest*)request success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure
{
	NSPersistentStoreCoordinator* coordinator = self.persistentStoreCoordinator;
	NSManagedObjectContext* backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	
	//Perform on background context
	[backgroundContext performBlock:^{
		
		//Set same persistent store coordinator (why does this have to happen in the background context performBlock?)
		backgroundContext.persistentStoreCoordinator = coordinator;
		
		//Execute fetch
		NSError* error;
		NSArray* fetchedObjects = [backgroundContext executeFetchRequest:request error:&error];
		if (error) {
			if (failure) {
				failure(error);
			}
			return;
		}
		
		//Perform on original context
		[self performBlock:^{
			
			if (fetchedObjects) {
				// Collect object IDs
				NSMutableArray* objectIds = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
				for (NSManagedObject* obj in fetchedObjects) {
					[objectIds addObject:obj.objectID];
				}
				
				// Fault in objects into current context by object ID as they are available in the shared persistent store
				NSMutableArray* objects = [[NSMutableArray alloc] initWithCapacity:[objectIds count]];
				for (NSManagedObjectID* objectId in objectIds) {
					NSManagedObject* object = [self objectWithID:objectId];
					[objects addObject:object];
				}
				
				//Send an immutable copy of objects array to success block
				if (success) {
					success([objects copy]);
				}
			}
		}];
	}];
}

@end
