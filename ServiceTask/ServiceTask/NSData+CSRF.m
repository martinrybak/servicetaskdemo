//
//  NSData+CSRF.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

NSString* const csrfString = @")]}',\n";

#import "NSData+CSRF.h"

@implementation NSData (CSRF)

/**
 *  Removes the cross-site scripting protection prefix )]}' at the beginning of response, if exists
 *
 *  @return cleaned NSData
 */
- (NSData*)removeCSRFPrefix
{
	NSString* responseString = [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
	NSString* leadString = [responseString substringToIndex:6];
	if ([leadString isEqualToString:csrfString]) {
		NSString* cleanedResponseString = [responseString stringByReplacingOccurrencesOfString:csrfString withString:@""];
		return [cleanedResponseString dataUsingEncoding:NSUTF8StringEncoding];
	}
	return self;
}

@end
