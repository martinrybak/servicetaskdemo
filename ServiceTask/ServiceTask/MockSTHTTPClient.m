//
//  MockSTHTTPClient.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "MockSTHTTPClient.h"

@implementation MockSTHTTPClient

- (void)get:(NSString*)endpoint query:(NSDictionary*)query cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure
{
	if ([endpoint isEqualToString:@"sync"]) {
		NSString* filePath = [[NSBundle mainBundle] pathForResource:@"MockSyncResponse" ofType:@"json"];
		NSData* data = [NSData dataWithContentsOfFile:filePath];
		//Parse JSON
		NSError* error;
		id object = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
		if (error) {
			if (failure) {
				failure(error);
			}
			return;
		}
		if (success) {
			success(nil, object);
		}
	}
}

- (void)post:(NSString*)endpoint query:(NSDictionary*)query body:(NSDictionary*)body cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure
{
}

@end
