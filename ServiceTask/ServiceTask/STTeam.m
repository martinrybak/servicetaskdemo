//
//  STTeam.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTeam.h"


@implementation STTeam

@dynamic color;
@dynamic createdAt;
@dynamic operation;
@dynamic guid;
@dynamic name;
@dynamic serverId;
@dynamic updatedAt;

@end
