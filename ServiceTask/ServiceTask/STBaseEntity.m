//
//  STBaseEntity.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/28/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STBaseEntity.h"
#import "STSyncResponseOperation.h"

NSString* const guidKey = @"guid";
NSString* const operationKey = @"operation";
NSString* const createdAtKey = @"createdAt";
NSString* const updatedAtKey = @"updatedAt";

@implementation STBaseEntity

@dynamic serverId;
@dynamic guid;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic operation;

#pragma mark - Public

- (void)mapWithSyncResponseOperation:(STSyncResponseOperation*)operation dateFormatter:(ISO8601DateFormatter*)dateFormatter
{
	self.createdAt = operation.createdAt;
	self.updatedAt = operation.updatedAt;
	self.serverId = operation.serverId;
	self.guid = operation.guid;
}

- (NSDictionary*)dictionaryWithDateFormatter:(ISO8601DateFormatter*)dateFormatter
{
	return nil;
}

/**
 *  Updates the following fields when a record is created by a user:
 *		-createdAt
 *		-updatedAt
 *		-operation
 *		-guid
 *  Throws an error if this method is called on an object with operation already set.
 */
- (void)create
{
	STOperationType operation = [[self valueForKeyPath:operationKey] integerValue];
	if (operation) {
		NSAssert(0, @"Cannot perform this operation on an entity that has already been created.");
	}
	
	[self setValue:[NSDate date] forKey:createdAtKey];
	[self setValue:[NSDate date] forKey:updatedAtKey];
	[self setValue:@(STOperationTypeCreate) forKey:operationKey];
	[self setValue:[[[NSUUID UUID] UUIDString] lowercaseString] forKey:guidKey];
}

/**
 *  If the current operation is nil or Updated, sets operation to Updated.
 *  If the current operation is Created, does nothing.
 *  If the current operation is Deleted, throws error.
 *  If no errors, Updates the updatedAt timestamp.
 */
- (void)update
{
	STOperationType operation = [[self valueForKeyPath:operationKey] integerValue];
	switch (operation)
	{
		//Do nothing. Keep this record flagged as Create
		case STOperationTypeCreate:
			break;
		//Do nothing. Keep this record flagged as Update
		case STOperationTypeUpdate:
			break;
		//Throw an error. We should not be trying to update an object flagged as Delete
		case STOperationTypeDelete:
			NSAssert(0, @"Cannot update an entity flagged for deletion.");
			break;
		//Operation is nil, flag as Update
		default:
			[self setValue:@(STOperationTypeUpdate) forKeyPath:operationKey];
			break;
	}
	[self setValue:[NSDate date] forKey:updatedAtKey];
}

/**
 *  If the current operation is nil or Updated, sets operation to Deleted. Returns NO not to perform hard delete.
 *  If the current operation is Created, returns YES to perform hard delete.
 *  If the current operation is Deleted, returns NO not to perform hard delete.
 */
- (void)delete
{
	STOperationType operation = [[self valueForKeyPath:operationKey] integerValue];
	switch (operation)
	{
		//Respond with hard delete
		case STOperationTypeCreate:
			break;
		//Flag as delete
		case STOperationTypeUpdate:
			[self setValue:@(STOperationTypeDelete) forKeyPath:operationKey];
			break;
		//Do nothing. Keep this record flagged as Delete
		case STOperationTypeDelete:
			break;
		//Operation is nil, flag as Delete
		default:
			[self setValue:@(STOperationTypeDelete) forKeyPath:operationKey];
			break;
	}
}

- (BOOL)shouldHardDelete
{
	STOperationType operation = [[self valueForKeyPath:operationKey] integerValue];
	switch (operation)
	{
		case STOperationTypeCreate:
			return YES;
		case STOperationTypeUpdate:
			return NO;
		case STOperationTypeDelete:
			return NO;
		default:
			return NO;
	}
}

/**
 *  Sets the operation 
 */
- (void)clearOperation
{
	[self setValue:@(STOperationTypeNone) forKey:operationKey];
}

@end
