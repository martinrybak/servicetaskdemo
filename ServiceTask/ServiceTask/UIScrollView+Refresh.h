//
//  UIScrollView+Refresh.h
//  Updated
//
//  Created by Martin Rybak on 3/15/14.
//  Copyright (c) 2014 Updated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Refresh)

@property (strong, nonatomic) UIRefreshControl* refreshControl;
@property (strong, nonatomic) UIView* refreshControlBackground;
- (void)addRefreshControlWithTarget:(id)target action:(SEL)action backgroundColor:(UIColor*)backgroundColor;
- (void)addRefreshControlWithTarget:(id)target action:(SEL)action;
- (void)endRefreshing;
- (void)removeRefreshControl;

@end