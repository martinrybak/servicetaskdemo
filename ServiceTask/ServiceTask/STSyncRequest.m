//
//  STSyncRequest.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STSyncRequest.h"
#import "STMeta.h"
#import "STSyncService.h"
#import "STSyncRequestInventory.h"
#import "STSyncRequestOperation.h"
#import "NSArray+LinqExtensions.h"
#import "NSArray+Concatenate.h"

@implementation STSyncRequest

@dynamic dateFormatter;

+ (instancetype)requestWithInventory:(NSArray*)inventory creates:(NSArray*)creates updates:(NSArray*)updates deletes:(NSArray*)deletes from:(NSDate*)from to:(NSDate*)to lastSyncDate:(NSDate*)lastSyncDate
{
	return [[self alloc] initWithInventory:inventory creates:creates updates:updates deletes:deletes from:from to:to lastSyncDate:lastSyncDate];
}

- (instancetype)initWithInventory:(NSArray*)inventory creates:(NSArray*)creates updates:(NSArray*)updates deletes:(NSArray*)deletes from:(NSDate*)from to:(NSDate*)to lastSyncDate:(NSDate*)lastSyncDate
{
	if (self = [super init]) {
		_from = from;
		_to = to;
		_lastSyncDate = lastSyncDate;
		_inventory = [self inventoryForObjects:inventory];
		_creates = [self operationsForObjects:creates type:STOperationTypeCreate];
		_updates = [self operationsForObjects:updates type:STOperationTypeUpdate];
		_deletes = [self operationsForObjects:deletes type:STOperationTypeDelete];
	}
	return self;
}

- (NSDictionary*)dictionary
{
	return @{
		@"timestamp":self.lastSyncDate ?: [NSNull null],
		@"from":self.from,
		@"to":self.to,
		@"inventory":[self dictionariesForInventory],
		@"operations":[self dictionariesForOperations]
	};
}

#pragma mark - Private

- (NSArray*)inventoryForObjects:(NSArray*)objects
{
	return [objects linq_select:^id(NSManagedObject* object) {
		return [STSyncRequestInventory inventoryForObject:object];
	}];
}

- (NSArray*)operationsForObjects:(NSArray*)objects type:(STOperationType)operationType
{
	return [objects linq_select:^id(NSManagedObject* object) {
		return [STSyncRequestOperation operationForObject:object operation:operationType];
	}];
}

- (NSArray*)dictionariesForInventory
{
	return [self.inventory linq_select:^id(STSyncRequestInventory* inventory) {
		return [inventory dictionary];
	}];
}

- (NSArray*)dictionariesForOperations
{
	NSArray* creates = [self.creates linq_select:^id(STSyncRequestOperation* operation) {
		return [operation dictionary];
	}];
	NSArray* updates = [self.updates linq_select:^id(STSyncRequestOperation* operation) {
		return [operation dictionary];
	}];
	NSArray* deletes = [self.deletes linq_select:^id(STSyncRequestOperation* operation) {
		return [operation dictionary];
	}];
	return [NSArray concatenate:@[ creates, updates, deletes ]];
}

@end
