//
//  STUser.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STUser.h"


@implementation STUser

@dynamic email;
@dynamic firstName;
@dynamic lastName;

@end
