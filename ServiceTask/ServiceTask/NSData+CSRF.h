//
//  NSData+CSRF.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (CSRF)

- (NSData*)removeCSRFPrefix;

@end
