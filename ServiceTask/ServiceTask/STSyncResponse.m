//
//  STSyncResponse.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STSyncResponse.h"
#import "STSyncResponseOperation.h"
#import "NSArray+LinqExtensions.h"
#import "STMeta.h"

@implementation STSyncResponse

@dynamic dateFormatter;

+ (instancetype)responseWithDictionary:(NSDictionary*)dictionary
{
	return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
	if (self = [super init]) {
		[self mapWithDictionary:dictionary];
	}
	return self;
}

- (void)mapWithDictionary:(NSDictionary*)dictionary
{
	self.timestamp = [self.dateFormatter dateFromString:dictionary[@"timestamp"]];
	self.from = [self.dateFormatter dateFromString:dictionary[@"from"]];
	self.to = [self.dateFormatter dateFromString:dictionary[@"to"]];
	self.operations = [dictionary[@"operations"] linq_select:^id(NSDictionary* dictionary) {
		return [STSyncResponseOperation operationWithDictionary:dictionary];
	}];
}

@end
