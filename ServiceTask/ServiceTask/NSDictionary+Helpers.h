//
//  NSDictionary+Helpers.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/8/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Helpers)

- (BOOL)isEmpty;

@end
