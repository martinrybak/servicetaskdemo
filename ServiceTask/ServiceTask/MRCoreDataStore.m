//
//  MRCoreDataStore.m
//
//  Created by Martin Rybak on 6/30/14.
//  Copyright (c) 2014 Martin Rybak. All rights reserved.
//

#import "MRCoreDataStore.h"
#import "NSArray+LinqExtensions.h"

typedef NS_ENUM(NSUInteger, MRCoreDataStoreQueue) {
    MRCoreDataStoreQueueForeground = 1,
    MRCoreDataStoreQueueBackground = 2
};

@implementation MRCoreDataStore

#pragma mark - NSObject

- (id)init
{
	if (self = [super init]) {
		[self setup];
	}
	return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public

//TODO: allow user to override database path, directory, and store options

- (void)setup
{
	//Set up CoreData managed object model
	NSString* bundleName = (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleNameKey];
	NSURL* modelURL = [[NSBundle mainBundle] URLForResource:bundleName withExtension:@"momd"];
	self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	NSAssert(self.managedObjectModel, @"Could not locate %@", [modelURL path]);
	
	//Set up CoreData persistent store coordinator
	self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
	
	//Set up CoreData persistent store (SQLite database)
	NSError* error;
	NSURL* storeURL = [self databasePath];
	[self findOrCreateDirectoryAtURL:[self databaseDirectory]];
	NSPersistentStore* store = [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:[self storeOptions] error:&error];
	NSAssert(store, @"Could not create persistent store: %@", [error localizedDescription]);
	
	//Create a foreground managed object context
	self.foregroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	self.foregroundContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
	//TODO: set merge policy?
	
	//Create a background managed object context
	self.backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	self.backgroundContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
	//TODO: set merge policy?
	
	//Register as an observer of CoreData managed object context saves
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backgroundContextDidSave:) name:NSManagedObjectContextDidSaveNotification object:self.backgroundContext];
}

- (NSURL*)databaseDirectory
{
	NSString* bundleName = (NSString *)[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleNameKey];
	NSURL* applicationSupportDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
	NSURL* databaseDirectory = [applicationSupportDirectory URLByAppendingPathComponent:bundleName];
	return databaseDirectory;
}

- (NSDictionary*)storeOptions
{
	return @{
		NSMigratePersistentStoresAutomaticallyOption:@(YES),	//Options key to automatically attempt to migrate versioned stores
		NSInferMappingModelAutomaticallyOption:@(YES),			//Coordinator will attempt to infer a mapping model if none can be found
		NSSQLitePragmasOption:@{ @"journal_mode":@"WAL" }		//Adding write-ahead logging mode recommended by apple
	};
}




- (NSFetchRequest*)fetchRequestForType:(Class)class
{
	return [self fetchRequestForType:class predicate:nil];
}

- (NSFetchRequest*)fetchRequestForType:(Class)class predicate:(NSPredicate*)predicate
{
	return [self fetchRequestForType:class predicate:predicate sortDescriptors:nil];
}

- (NSFetchRequest*)fetchRequestForType:(Class)class predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground|MRCoreDataStoreQueueBackground];
	NSManagedObjectContext* context = [self contextForCurrentQueue];
	NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
	fetchRequest.entity  = [NSEntityDescription entityForName:NSStringFromClass(class) inManagedObjectContext:context];
	fetchRequest.predicate = predicate;
	fetchRequest.sortDescriptors = sortDescriptors;
	return fetchRequest;
}






- (NSFetchedResultsController*)fetchedResultsControllerForType:(Class)class
{
	return [self fetchedResultsControllerForType:class predicate:nil];
}

- (NSFetchedResultsController*)fetchedResultsControllerForType:(Class)class predicate:(NSPredicate*)predicate
{
	return [self fetchedResultsControllerForType:class predicate:predicate sortDescriptors:nil];
}

- (NSFetchedResultsController*)fetchedResultsControllerForType:(Class)class predicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors
{
	NSFetchRequest* fetchRequest = [self fetchRequestForType:class predicate:predicate sortDescriptors:sortDescriptors];
	return [self fetchedResultsControllerWithFetchRequest:fetchRequest];
}


/**
 *  Returns a fetched results controller for the given fetch request on the foreground context.
 *  It creates a cache for each unique fetch request.
 *
 *  @param fetchRequest NSFetchRequest
 *
 *  @return NSFetchedResultsController
 */
- (NSFetchedResultsController*)fetchedResultsControllerWithFetchRequest:(NSFetchRequest*)fetchRequest
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground];
	NSString* cacheName = [NSString stringWithFormat:@"%lu", (unsigned long)[fetchRequest hash]];
	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.foregroundContext sectionNameKeyPath:nil cacheName:nil];
}






- (id)localObjectForObject:(NSManagedObject*)object
{
	return [self findObjectById:object.objectID];
}

- (id)findObjectById:(NSManagedObjectID*)objectId
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground|MRCoreDataStoreQueueBackground];
	NSManagedObjectContext* context = [self contextForCurrentQueue];
	return [context objectWithID:objectId];
}

- (id)findObjectOfType:(Class)class byAttribute:(NSString*)attribute withValue:(id)value
{
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K == %@", attribute, value];
	return [self findObjectOfType:class byPredicate:predicate];
}

- (id)findObjectOfType:(Class)class byPredicate:(NSPredicate*)predicate
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground|MRCoreDataStoreQueueBackground];
	NSManagedObjectContext* context = [self contextForCurrentQueue];
	NSFetchRequest* fetchRequest = [self fetchRequestForType:class predicate:predicate];
	fetchRequest.fetchLimit = 1;
	NSError* error;
	id output = [[context executeFetchRequest:fetchRequest error:&error] firstObject];
	if (error) {
		NSLog(@"%@", [error localizedDescription]);
	}
	return output;
}




- (NSArray*)findObjectsOfType:(Class)class
{
	return [self findObjectsOfType:class byPredicate:nil];
}

- (NSArray*)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate
{
	return [self findObjectsOfType:class byPredicate:predicate sortDescriptors:nil];
}

- (NSArray*)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground|MRCoreDataStoreQueueBackground];
	NSFetchRequest* fetchRequest = [self fetchRequestForType:class predicate:predicate sortDescriptors:sortDescriptors];
	NSError* error;
	NSArray* output = [self.backgroundContext executeFetchRequest:fetchRequest error:&error];
	if (error) {
		NSLog(@"%@", [error localizedDescription]);
	}
	return output;
}




- (void)findObjectsOfType:(Class)class success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure
{
	[self findObjectsOfType:class byPredicate:nil success:success failure:failure];
}

- (void)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure
{
	[self findObjectsOfType:class byPredicate:predicate sortDescriptors:nil success:success failure:failure];
}

- (void)findObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate sortDescriptors:(NSArray*)sortDescriptors success:(void(^)(NSArray* objects))success failure:(void(^)(NSError* error))failure
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground];
	
	//Execute fetch in background context and convert results to foreground context
	[self.backgroundContext performBlock:^{
		
		NSArray* fetchedObjects = [self findObjectsOfType:class byPredicate:predicate sortDescriptors:sortDescriptors];
		[self.foregroundContext performBlock:^{
	
			NSArray* output = [fetchedObjects linq_select:^id(NSManagedObject* object) {
				return [self.foregroundContext objectWithID:object.objectID];
			}];
			if (success) {
				success([output copy]);
			}
		}];
	}];
}




- (id)findOrCreateObjectOfType:(Class)class byAttribute:(NSString*)attribute withValue:(id)value
{
	NSManagedObject* object = [self findObjectOfType:class byAttribute:attribute withValue:value];
	if(!object) {
		object = [self createObjectOfType:class];
		[object setValue:value forKey:attribute];
	}
	return object;
}

- (id)createObjectOfType:(Class)class
{
	[self restrictToQueue:MRCoreDataStoreQueueBackground];
	return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(class) inManagedObjectContext:self.backgroundContext];
}

- (void)deleteObject:(NSManagedObject*)object
{
	[self restrictToQueue:MRCoreDataStoreQueueBackground];
	
	NSManagedObject* backgroundObject;
	if (object.managedObjectContext == self.backgroundContext) {
		backgroundObject = object;
	} else {
		backgroundObject = [self findObjectById:object.objectID];
	}
	
	[self.backgroundContext deleteObject:backgroundObject];
}

- (void)deleteObjectOfType:(Class)class byAttribute:(NSString*)attribute withValue:(id)value
{
	NSManagedObject* object = [self findObjectOfType:class byAttribute:attribute withValue:value];
	[self deleteObject:object];
}

- (void)deleteObjectsOfType:(Class)class
{
	[self deleteObjectsOfType:class byPredicate:nil];
}

- (void)deleteObjectsOfType:(Class)class byPredicate:(NSPredicate*)predicate
{
	[self restrictToQueue:MRCoreDataStoreQueueBackground];
	
	NSFetchRequest* fetchRequest = [self fetchRequestForType:class predicate:predicate];
	fetchRequest.includesPropertyValues = NO; //only fetch the managedObjectID
	
	NSError* error;
	NSArray* objects = [self.backgroundContext executeFetchRequest:fetchRequest error:&error];
	if (error) {
		NSLog(@"%@", [error localizedDescription]);
	}
	for (NSManagedObject* object in objects) {
		[self.backgroundContext deleteObject:object];
	}
}




- (void)updateWithBlock:(void(^)(void))block completion:(void(^)(void))completion
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground];
	[self.backgroundContext performBlock:^{
		if (block) {
			block();
		}
		if (completion) {
			completion();
		}
	}];
}

- (void)commitWithSuccess:(void(^)(void))success failure:(void(^)(NSError* error))failure
{
	[self updateAndCommitWithBlock:nil success:success failure:failure];
}

- (void)updateAndCommitWithBlock:(void(^)(void))block success:(void(^)(void))success failure:(void(^)(NSError* error))failure
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground];
	[self.backgroundContext performBlock:^{
		if (block) {
			block();
		}
		
		NSError* error;
		if([self.backgroundContext save:&error]) {
			[self.foregroundContext performBlock:^{
				if (success) {
					success();
				}
			}];
		} else {
			[self.foregroundContext performBlock:^{
				if (failure) {
					failure(error);
				}
			}];
		}
	}];
}

- (void)rollback:(void(^)(void))completion
{
	[self restrictToQueue:MRCoreDataStoreQueueForeground];
	[self.backgroundContext performBlock:^{
		[self.backgroundContext rollback];
		[self.foregroundContext performBlock:^{
			if (completion) {
				completion();
			}
		}];
	}];
}

#pragma mark - NSNotificationCenter

//Merges changes into the foreground context, then posts a save notification.
- (void)backgroundContextDidSave:(NSNotification*)notification
{
	[self.foregroundContext performBlock:^{
		[self.foregroundContext mergeChangesFromContextDidSaveNotification:notification];
	}];
}

#pragma mark - Private

- (NSURL*)databasePath
{
	NSURL* databaseDirectory = [self databaseDirectory];
	NSString* bundleId = (NSString*)[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleIdentifierKey];
	NSURL* databaseFile = [databaseDirectory URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", bundleId]];
	return databaseFile;
}

- (void)findOrCreateDirectoryAtPath:(NSString*)path
{
	//Check if database directory exists
	if ([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NULL]) {
		return;
	}
	
	//Doesn't exist. Create it.
	NSError* error;
	if (![[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error]) {
		NSLog(@"%@", error.localizedDescription);
		abort();
	}
}

- (void)findOrCreateDirectoryAtURL:(NSURL*)URL
{
	[self findOrCreateDirectoryAtPath:[URL relativePath]];
}

- (void)restrictToQueue:(MRCoreDataStoreQueue)queue
{
	if (queue & MRCoreDataStoreQueueForeground && queue & MRCoreDataStoreQueueBackground) {
		NSAssert(
			[self isQueueForContext:self.foregroundContext] || [self isQueueForContext:self.backgroundContext],
			@"This method can only be called from the main thread or background context thread"
		);
		return;
	}
	if (queue & MRCoreDataStoreQueueForeground) {
		NSAssert([self isQueueForContext:self.foregroundContext], @"This method can only be called from the main thread.");
		return;
	}
	if (queue & MRCoreDataStoreQueueBackground) {
		NSAssert([self isQueueForContext:self.backgroundContext], @"This method can only be called from the background context thread");
		return;
	}
}

- (BOOL)isQueueForContext:(NSManagedObjectContext*)context
{
	NSParameterAssert(context);
	
	//NSConfinementConcurrencyType
	NSAssert(context.concurrencyType != NSConfinementConcurrencyType, @"A context of type NSConfinementConcurrencyType does not manage its own queue");
	
	//NSMainQueueConcurrencyType
	if ([NSThread isMainThread] && context.concurrencyType == NSMainQueueConcurrencyType) {
		return true;
	}
	
	//NSPrivateQueueConcurrencyType
	__block dispatch_queue_t queue;
	[context performBlockAndWait:^{
		queue = dispatch_get_current_queue();
	}];
	return dispatch_get_current_queue() == queue;
}

- (NSManagedObjectContext*)contextForCurrentQueue
{
	if ([NSThread isMainThread]) {
		return self.foregroundContext;
	}
	if ([self isQueueForContext:self.backgroundContext]) {
		return self.backgroundContext;
	}
	return nil;
}

@end