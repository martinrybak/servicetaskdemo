//
//  STSyncOperation.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISO8601DateFormatter.h"
#import <CoreData/CoreData.h>
#import <BloodMagic/Lazy.h>
#import "STSyncService.h"
#import "STBaseEntity.h"

@interface STSyncResponseOperation : NSObject <BMLazy>

@property (weak, nonatomic) ISO8601DateFormatter* dateFormatter;
@property (strong, nonatomic) Class type;
@property (strong, nonatomic) NSNumber* serverId;
@property (copy, nonatomic) NSString* guid;
@property (assign, nonatomic) STOperationType operation;
@property (strong, nonatomic) NSDate* createdAt;
@property (strong, nonatomic) NSDate* updatedAt;
@property (strong, nonatomic) NSDictionary* data;

+ (instancetype)operationWithDictionary:(NSDictionary*)dictionary;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
- (void)mapWithDictionary:(NSDictionary*)dictionary;

@end
