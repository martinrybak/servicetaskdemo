//
//  STAuthService.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/26/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STAuthService.h"
#import "STUser+Mapping.h"

NSString* const loginEndpoint = @"api/auth";
NSString* const tokenKey = @"token";

@implementation STAuthService

@dynamic store;
@dynamic client;
@dynamic keychain;
@dynamic dateFormatter;

#pragma mark - Public

- (void)loginWithEmail:(NSString*)email password:(NSString*)password success:(void(^)(void))success failure:(void(^)(NSError* error))failure
{
	NSParameterAssert(email);
	NSParameterAssert(password);
	
    NSDictionary* parameters = @{
		@"username":email,
		@"password":password,
		@"remember":@(NO),
		@"recaptcha":[NSNull null]
	};
	
	[self.client post:loginEndpoint query:nil body:parameters cookie:nil success:^(NSDictionary* headers, NSDictionary* response) {
		NSString* token = [self extractTokenFromHeaders:headers];
		[self.keychain setSecret:token forKey:tokenKey];
		[self.store updateAndCommitWithBlock:^{
			STUser* user = [self.store createObjectOfType:[STUser class]];
			[user mapWithDictionary:response dateFormatter:self.dateFormatter];
		} success:success failure:failure];
	} failure:failure];
}

- (NSString*)token
{
	return [self.keychain secretForKey:tokenKey];
}

#pragma mark - Private

- (NSString*)extractTokenFromHeaders:(NSDictionary*)headers
{
	NSString* cookies = headers[@"Set-Cookie"];
	NSError* error;
	NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"ST_SESSION=(.*?);" options:NSRegularExpressionCaseInsensitive error:&error];
	if (error) {
		return nil;
	}
	NSArray* matches = [regex matchesInString:cookies options:0 range: NSMakeRange(0, cookies.length)];
	NSTextCheckingResult* match = matches[0];
	NSString* token = [cookies substringWithRange:[match range]];
	return token;
}

@end
