//
//  NSDate+Add.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "NSDate+Add.h"

@implementation NSDate (Add)

- (NSDate*)addDays:(NSUInteger)days
{
	return [self dateByAddingTimeInterval:[self secondsInDay] * days];
}

- (NSDate*)subtractDays:(NSUInteger)days
{
	return [self dateByAddingTimeInterval:[self secondsInDay] * days * -1];
}

- (NSTimeInterval)secondsInDay
{
	return 60 * 60 * 24;
}

@end
