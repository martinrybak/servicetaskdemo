//
//  STSyncService.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STSyncService.h"
#import "STTask.h"
#import "STTeam.h"
#import "STMeta.h"
#import "NSDate+Add.h"
#import "NSObject+JSON.h"
#import "STBaseEntity.h"
#import "STSyncRequest.h"
#import "STSyncResponse.h"
#import "STSyncResponseOperation.h"

NSString* const syncEndpoint = @"sync";

@implementation STSyncService

@dynamic store;
@dynamic client;
@dynamic dateFormatter;

#pragma mark - Public

+ (Class)classForString:(NSString*)string
{
	if ([string isEqualToString:@"task"]) {
		return [STTask class];
	}
	if ([string isEqualToString:@"team"]) {
		return [STTeam class];
	}
	return nil;
}

+ (NSString*)stringForClass:(Class)class
{
	if (class == [STTask class]) {
		return @"task";
	}
	if (class == [STTeam class]) {
		return @"team";
	}
	return nil;
}

+ (STOperationType)operationForString:(NSString*)string
{
	if ([string isEqualToString:@"create"]) {
		return STOperationTypeCreate;
	}
	if ([string isEqualToString:@"update"]) {
		return STOperationTypeUpdate;
	}
	if ([string isEqualToString:@"delete"]) {
		return STOperationTypeDelete;
	}
	return 0;
}

+ (NSString*)stringForOperation:(STOperationType)operation
{
	switch (operation) {
		case STOperationTypeCreate:
			return @"create";
			break;
		case STOperationTypeUpdate:
			return @"update";
			break;
		case STOperationTypeDelete:
			return @"delete";
			break;
		default:
			return nil;
			break;
	}
}

- (void)sync:(void(^)(void))success failure:(void(^)(NSError* error))failure
{
	NSDate* from = [[NSDate date] subtractDays:7];
	NSDate* to = [[NSDate date] addDays:14];
	NSArray* inventory = [self getInventory];
	NSArray* creates = [self getOperationsOfType:STOperationTypeCreate];
	NSArray* updates = [self getOperationsOfType:STOperationTypeUpdate];
	NSArray* deletes = [self getOperationsOfType:STOperationTypeDelete];
	NSDate* lastSyncDate = [self lastSyncDate];
	STSyncRequest* syncRequest = [STSyncRequest requestWithInventory:inventory creates:creates updates:updates deletes:deletes from:from to:to lastSyncDate:lastSyncDate];
	NSDictionary* body = [syncRequest dictionary];
	
	#if DEBUG
	NSString* print = [[NSString alloc] initWithData:[body JSON] encoding:NSUTF8StringEncoding];
	NSLog(@"%@", print);
	#endif
	
	[self.client post:syncEndpoint query:@{ @"id":@1 } body:body cookie:nil success:^(NSDictionary* headers, NSDictionary* response) {
		STSyncResponse* syncResponse = [STSyncResponse responseWithDictionary:response];
		[self processSyncResponse:syncResponse success:success failure:failure];
	} failure:failure];
}

- (NSDate*)lastSyncDate
{
	STMeta* meta = [[self.store findObjectsOfType:[STMeta class]] firstObject];
	return meta.lastSyncedAt;
}

#pragma mark - Private

- (NSArray*)allTypes
{
	return @[ [STTask class], [STTeam class] ];
}

- (void)processSyncResponse:(STSyncResponse*)response success:(void(^)(void))success failure:(void(^)(NSError* error))failure
{
	for (STSyncResponseOperation* operation in response.operations) {
		[self processSyncResponseOperation:operation];
	}
	
	//Only if save is successful, update last sync date
	[self.store commitWithSuccess:^{
		[self updateLastSyncDateFromResponse:response];
		if (success) {
			success();
		}
	} failure:failure];
}

- (void)processSyncResponseOperation:(STSyncResponseOperation*)responseOperation
{
	[self.store updateWithBlock:^{
		
		STBaseEntity* object;
		switch (responseOperation.operation)
		{
			case STOperationTypeCreate:
			{
				object = [self.store createObjectOfType:responseOperation.type];
				break;
			}
			case STOperationTypeUpdate:
			{
				if (responseOperation.guid) {
					object = [self.store findObjectOfType:responseOperation.type byAttribute:@"guid" withValue:responseOperation.guid];
				} else {
					object = [self.store findObjectOfType:responseOperation.type byAttribute:@"serverId" withValue:responseOperation.serverId];
				}
				break;
			}
			case STOperationTypeDelete:
			{
				if (responseOperation.guid) {
					[self.store deleteObjectOfType:responseOperation.type byAttribute:@"guid" withValue:responseOperation.guid];
				} else {
					[self.store deleteObjectOfType:responseOperation.type byAttribute:@"serverId" withValue:responseOperation.serverId];
				}
				return;
			}
			default:
				break;
		}
		
		[object mapWithSyncResponseOperation:responseOperation dateFormatter:self.dateFormatter];
		[object clearOperation];
	} completion:nil];
}

- (NSArray*)getInventory
{
	NSMutableArray* output = [NSMutableArray array];
	for (Class class in [self allTypes]) {
		NSArray* array =  [self.store findObjectsOfType:class];
		[output addObjectsFromArray:array];
	}
	return [output copy];
}

- (NSArray*)getOperationsOfType:(STOperationType)type
{
	NSMutableArray* output = [NSMutableArray array];
	for (Class class in [self allTypes]) {
		NSPredicate* predicate = [NSPredicate predicateWithFormat:@"operation = %@", @(type)];
		NSArray* array =  [self.store findObjectsOfType:class byPredicate:predicate];
		[output addObjectsFromArray:array];
	}
	return [output copy];
}

- (void)updateLastSyncDateFromResponse:(STSyncResponse*)response
{
	[self.store updateAndCommitWithBlock:^{
		STMeta* meta;
		meta = [[self.store findObjectsOfType:[STMeta class]] firstObject];
		if (!meta) {
			meta = [self.store createObjectOfType:[STMeta class]];
		}
		meta.lastSyncedAt = response.timestamp;
	} success:nil failure:nil];
}

@end
