//
//  STUser+Helpers.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STUser+Helpers.h"

@implementation STUser (Helpers)

- (NSString*)fullName
{
	return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

@end
