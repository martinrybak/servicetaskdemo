//
//  MockSTHTTPClient.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STHTTPClient.h"

@interface MockSTHTTPClient : STHTTPClient

- (void)get:(NSString*)endpoint query:(NSDictionary*)query cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure;
- (void)post:(NSString*)endpoint query:(NSDictionary*)query body:(NSDictionary*)body cookie:(NSString*)cookie success:(void(^)(NSDictionary* headers, id response))success failure:(void(^)(NSError* error))failure;

@end
