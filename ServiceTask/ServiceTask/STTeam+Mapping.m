//
//  STTeam+Mapping.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/20/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTeam+Mapping.h"

@implementation STTeam (Mapping)

- (void)mapWithSyncResponseOperation:(STSyncResponseOperation*)operation dateFormatter:(ISO8601DateFormatter*)dateFormatter
{
	self.createdAt = operation.createdAt;
	self.updatedAt = operation.updatedAt;
	self.serverId = operation.serverId;
	self.guid = operation.guid;
	self.name = operation.data[@"name"];
	self.color = operation.data[@"color"];
}

- (NSDictionary*)dictionaryWithDateFormatter:(ISO8601DateFormatter*)dateFormatter
{
	return @{
		@"name":self.name,
		@"color":self.color
	};
}

@end
