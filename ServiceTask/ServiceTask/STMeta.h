//
//  STMeta.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface STMeta : NSManagedObject

@property (nonatomic, retain) NSDate * lastSyncedAt;
@property (nonatomic, retain) NSDate * tokenExpiresAt;
@property (nonatomic, retain) NSDate * tokenIssuedAt;

@end
