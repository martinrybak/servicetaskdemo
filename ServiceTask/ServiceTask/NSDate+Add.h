//
//  NSDate+Add.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Add)

- (NSDate*)addDays:(NSUInteger)days;
- (NSDate*)subtractDays:(NSUInteger)days;

@end
