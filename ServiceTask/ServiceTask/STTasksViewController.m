//
//  STTasksViewController.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STTasksViewController.h"
#import "STTask.h"
#import "UIAlertView+BlocksKit.h"
#import "UIScrollView+Refresh.h"
#import "JDStatusBarNotification.h"
#import "NSObject+BKBlockExecution.h"
#import "UITextField+BlocksKit.h"
#import "Reachability.h"
#import "STTeam.h"

@interface STTasksViewController ()

@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;

@end

@implementation STTasksViewController

@dynamic taskService;
@dynamic syncService;

#pragma mark - STBaseViewController

- (void)setup
{
	[super setup];
	self.fetchedResultsController = [self.taskService tasksFetchedResultsController];
    self.fetchedResultsController.delegate = self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self.tableView addRefreshControlWithTarget:self action:@selector(refresh)];
	[self.fetchedResultsController performFetch:nil];
}

#pragma mark - Public

- (void)addTask
{
	[self showPromptForTask:nil];
}

- (void)editTask:(STTask*)task
{
	[self showPromptForTask:task];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* const identifier = @"cell";
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
	[self configureCell:cell atIndexPath:indexPath];
	return cell;
}

- (void)configureCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
	STTask* task = self.fetchedResultsController.fetchedObjects[indexPath.row];
	cell.textLabel.text = [NSString stringWithFormat:@"%@", task.name];
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath*)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		STTask* task = self.fetchedResultsController.fetchedObjects[indexPath.row];
		[self.taskService deleteTask:task];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	STTask* task = self.fetchedResultsController.fetchedObjects[indexPath.row];
	[self editTask:task];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController*)controller
{
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController*)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath*)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath*)newIndexPath
{
    switch(type)
	{
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
    }
}

#pragma mark - Private

- (void)showPromptForTask:(STTask*)task
{
	NSString* title = @"New Task";
	if (task) {
		title = @"Edit Task";
	}
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
	UITextField* textField = [alertView textFieldAtIndex:0];
	[textField setBk_shouldReturnBlock:^BOOL(UITextField* textField) {
		[textField resignFirstResponder];
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
		return YES;
	}];
	if (task) {
		textField.text = task.name;
	}
	[alertView bk_setWillDismissBlock:^(UIAlertView* alertView, NSInteger buttonIndex) {
		if (buttonIndex || textField.text.length) {
			if (task) {
				[self.taskService updateTask:task withName:textField.text];
			} else {
				[self.taskService createTaskWithName:textField.text];
			}
		}
	}];
	[alertView show];
}

- (void)refresh
{
	if (![self.delegate online]) {
		[self.tableView endRefreshing];
		NSError* error = [NSError errorWithDomain:NSStringFromClass([self class]) code:-1 userInfo:@{ NSLocalizedDescriptionKey:@"No internet connection" }];
		[self.delegate failure:error];
		return;
	}
	
	[JDStatusBarNotification showWithStatus:@"Syncing..." styleName:JDStatusBarStyleSuccess];
	[NSObject bk_performBlock:^{
		[self.syncService sync:^{
			[JDStatusBarNotification dismiss];
			[self.tableView endRefreshing];
		} failure:^(NSError* error) {
			[JDStatusBarNotification dismiss];
			[self.tableView endRefreshing];
			[self.delegate failure:error];
		}];
	} afterDelay:1.0];
}

@end
