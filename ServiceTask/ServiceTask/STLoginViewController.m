//
//  STLoginViewController.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STLoginViewController.h"
#import "MBProgressHUD.h"
#import "STUser+Helpers.h"

@interface STLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField* emailField;
@property (weak, nonatomic) IBOutlet UITextField* passwordField;

@end

@implementation STLoginViewController

@dynamic authService;
@dynamic syncService;

- (IBAction)loginTapped:(id)sender
{
	[self.delegate busy:YES];
	[self.authService loginWithEmail:self.emailField.text password:self.passwordField.text success:^{
		[self.delegate busy:NO];
		[[[UIAlertView alloc] initWithTitle:@"Welcome" message:@"Success" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
	} failure:^(NSError* error) {
		[self.delegate busy:NO];
		[[[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
	}];
}

- (IBAction)syncTapped:(id)sender
{
	[MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
	[self.syncService sync:^{
		[MBProgressHUD hideHUDForView:self.view.window animated:YES];
		[[[UIAlertView alloc] initWithTitle:@"Success"
									message:nil
								   delegate:nil
						  cancelButtonTitle:@"Ok"
						  otherButtonTitles:nil] show];
	} failure:^(NSError* error) {
		[MBProgressHUD hideHUDForView:self.view.window animated:YES];
		[[[UIAlertView alloc] initWithTitle:@"Error"
									message:[error localizedDescription]
								   delegate:nil
						  cancelButtonTitle:@"Ok"
						  otherButtonTitles:nil] show];
	}];
}

@end
