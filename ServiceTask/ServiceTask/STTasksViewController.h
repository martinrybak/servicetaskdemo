//
//  STTasksViewController.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STBaseViewController.h"
#import <BloodMagic/Lazy.h>
#import "STTaskService.h"
#import "STSyncService.h"

@interface STTasksViewController : STBaseViewController <BMLazy, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) STTaskService* taskService;
@property (weak, nonatomic) STSyncService* syncService;

- (void)addTask;
- (void)editTask:(STTask*)task;

@end
