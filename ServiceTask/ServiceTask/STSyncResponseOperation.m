//
//  STSyncOperation.m
//  ServiceTask
//
//  Created by Martin Rybak on 8/21/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import "STSyncResponseOperation.h"
#import "STTask.h"
#import "STTeam.h"

@implementation STSyncResponseOperation

@dynamic dateFormatter;

+ (instancetype)operationWithDictionary:(NSDictionary*)dictionary
{
	return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
	if (self = [super init]) {
		[self mapWithDictionary:dictionary];
	}
	return self;
}

- (void)mapWithDictionary:(NSDictionary*)dictionary
{
	_type = [STSyncService classForString:dictionary[@"type"]];
	_operation = [STSyncService operationForString:dictionary[@"op"]];
	_createdAt = [self.dateFormatter dateFromString:dictionary[@"created"]];
	_updatedAt = [self.dateFormatter dateFromString:dictionary[@"updated"]];
	_serverId = dictionary[@"id"];
	_guid = dictionary[@"guid"];
	_data = dictionary[@"data"];
}

@end
