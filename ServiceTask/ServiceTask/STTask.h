//
//  STTask.h
//  ServiceTask
//
//  Created by Martin Rybak on 8/27/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "STBaseEntity.h"

@interface STTask : STBaseEntity

@property (nonatomic, retain) NSNumber * completed;
@property (nonatomic, retain) NSDate * dueAt;
@property (nonatomic, retain) NSString * name;

@end
