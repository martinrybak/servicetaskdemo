//
//  ServiceTaskTests.m
//  ServiceTaskTests
//
//  Created by Martin Rybak on 8/7/14.
//  Copyright (c) 2014 ServiceTask. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface ServiceTaskTests : XCTestCase

@end

@implementation ServiceTaskTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPass
{
	XCTAssert(1 == 1, @"One equals one. All is sane in the world.");
}

- (void)testFail
{
	XCTAssert(1 == 2, @"One does not equal two.");
}

@end
